<?php
require_once 'config/config.php';
require_once 'modules/db.php';
require_once 'modules/template.php';

  db::connect();
  db::check_tables();

  $rule = NOT_AUTHORIZED_USER;

  if($rule == NOT_AVAILABLE)
  {
    print "Нет необходимых прав для доступа к данной странице";
  }
  else
  {
    $t = new tpl;
    $t->make_head('CarExpenses', array(["rel"=>'stylesheet', "href"=>'dist/css/bootstrap.min.css'], ["rel"=>'stylesheet', "href"=>'css/style.css']));
    $t->make_body(array());
    $t->link_scripts(array(["type"=>"text/javascript", "src"=>"dist/js/jquery-3.2.1.slim.min.js"], ["type"=>"text/javascript", "src"=>"dist/js/popper.min.js"], ["type"=>"text/javascript", "src"=>"dist/js/vue.js"], ["type"=>"text/javascript", "src"=>"dist/js/axios.min.js"], ["type"=>"text/javascript", "src"=>"dist/js/bootstrap.min.js"], ["type"=>"text/javascript", "src"=>"js/app.js"]));
  }

  //db::connect();
  //db::create("garage");
  //db::add('sdfgh', 'sdfg', '20.02.2012', '12345');
  /*$result = db::get("garage");
  if($result === null)
    echo "NULL";
  else
  {
    while ($row = $result->fetch_assoc())
    {
      echo $row['id'] . $row['brand'] . $row['model'] . $row['year'] . $row['mileage'];
    }
  }*/
 ?>
