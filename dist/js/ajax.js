'use strict';

window.onload = function(){
	$("#getUsers").on("click",function(e){
		e.preventDefault();
		$.post(
			'/',
			{
				"ajax":"true",
				"module":"test_mod",
				"method":"get_users",
				"params":{}
			},
			onTestRequestDone);

		function onTestRequestDone(data){
			alert("Данные с сервера: " + data.toString());
		};
	});

	$("#getGroup").on("click",function(e){
		e.preventDefault();
		$.post(
			'/',
			{
				"ajax":"true",
				"module":"test_mod",
				"method":"get_groups",
				"params":{}
			},
			onTestRequestDone);

			function onTestRequestDone(data){
				alert("Данные с сервера: " + data.toString());
			};
});

$("#blabla").on("click",function(e){
	e.preventDefault();
	$.post(
		'/',
		{
			"ajax":"true",
			"module":"test_mod",
			"method":"blabla",
			"params":{}
		},
		onTestRequestDone);

		function onTestRequestDone(data){
			alert("Данные с сервера: " + data.toString());
		};
});

}
