// Главная страница приложения
Vue.component('main-page',
  {
    template:'\
    <div :class="hiddened">\
      <h1>Main Page</h1>\
    </div>\
    ',
    props:["hidden_main","user_rule"],
    computed:{
      hiddened:function(){
        if(this.hidden_main === true)
        {
          return 'hidden'
        }
        else {
          return ''
        }
      }
    }
  }
)

// Модальное окно входа
Vue.component('login',
  {
    template:'\
    <div class="modal db-example-modal-sm" id="login" tabindex="-1" role="dialog" aria-labelledy="login" :style="hiddened">\
      <div class="modal-dialog modal-sm" role="document">\
        <div class="modal-content">\
          <div class="modal-header">\
            <h5 class="modal-title" id="add-gsm-label">Вход</h5>\
            <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.stop.prevent="close"><span aria-hidden="true">&times;</span></button>\
          </div>\
          <div class="modal-body">\
            <label > Логин </label>\
            <input class="form-control" :class="login_val" type="text" v-model="login"></input>\
            <label > Пароль </label>\
            <input class="form-control" :class="password_val" type="password" v-model="password"></input>\
          </div>\
          <div class="modal-footer">\
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="login_user">Войти</button>\
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="close">Отмена</button>\
          </div>\
        </div>\
      </div>\
    </div>\
    ',
    props: ['hidden_login'],
    data: function(){
      return{
        login: "",
        login_val: "",
        password: "",
        password_val: ""
      }
    },
    computed:{
      hiddened: function(){
        if(this.hidden_login === true)
        {
          return 'display: none'
        }
        else {
          return 'display: block'
        }
      }
    },
    methods:{
      close: function(){
        this.$emit('close_login');
        this.login = '';
        this.login_val = '';
        this.password = '';
        this.password_val = '';
      },
      login_user: function(){
        if(this.login === "" || this.password === "")
        {
          if(this.login === "")
            this.login_val = 'error'
          else
            this.login_val = ''

          if(this.password === "")
            this.password_val = 'error'
          else
            this.password_val = ''
          alert("Заполнены не все поля")
        }
        else{
          axios.post('modules/users.php', {method: 'login_user', login: this.login, password: this.password})
          .then(response => {this.$emit('user_logined', response.data.rule, response.data.id);
            this.close();})
          .catch(e => {alert("error " + e)})
        }
      }
    }
  }
)

// Модальное окно регистрации
Vue.component('registration',
  {
    template:'\
    <div class="modal db-example-modal-sm" id="add-gsm" tabindex="-1" role="dialog" aria-labelledy="advanced-search" :style="hiddened">\
      <div class="modal-dialog modal-sm" role="document">\
        <div class="modal-content">\
          <div class="modal-header">\
            <h5 class="modal-title" id="add-gsm-label">Регистрация</h5>\
            <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.stop.prevent="close"><span aria-hidden="true">&times;</span></button>\
          </div>\
          <p class="message-registration" v-for="message, index in messages" :key=index>{{messages[index]}}</p>\
          <div class="modal-body">\
            <label > Логин </label>\
            <input class="form-control" :class="login_val" type="text" v-model="login"></input>\
            <label > E-mail </label>\
            <input class="form-control" :class="email_val" type="email" v-model="email"></input>\
            <label > Пароль </label>\
            <input class="form-control" :class="password_val" type="password" v-model="password"></input>\
            <label > Подтверждение пароля </label>\
            <input class="form-control" :class="password_two_val" type="password" v-model="password_two"></input>\
          </div>\
          <div class="modal-footer">\
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="add">Добавить</button>\
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="close">Отмена</button>\
          </div>\
        </div>\
      </div>\
    </div>\
    ',
    props: ['hidden_registration'],
    data: function(){
      return{
        login_val: "",
        login: "",
        email_val: "",
        email: "",
        password_val: "",
        password: "",
        password_two_val: "",
        password_two: "",
        messages: [],
      }
    },
    computed:{
      hiddened: function(){
        if(this.hidden_registration === true)
        {
          return 'display: none'
        }
        else {
          return 'display: block'
        }
      }
    },
    methods:{
      close: function(){
        this.$emit('close_registration');
        this.login = '';
        this.login_val= '';
        this.email = '';
        this.email_val = '';
        this.password = '';
        this.password_val = '';
        this.password_two = '';
        this.password_two_val = '';
        this.messages = [];
      },
      add: function(){
        this.messages = [];
        if(this.login === "" || this.email === "" || this.password === "" || this.password_two === "" || this.password.length < 9)
        {
          if(this.login === "")
          {
            this.login_val = 'error';
            this.messages.push("Введите логин");
          }
          else
            this.login_val = ''
          if(this.email === "")
          {
            this.email_val = 'error';
            this.messages.push("Введите e-mail");
          }
          else
          {
            if(this.email.indexOf('@') + 1)
            {
              this.email_val = '';
            }
            else{
              this.email_val = 'error';
              this.messages.push("E-mail введен не верно")
            }

          }
          if(this.password === "")
          {
            this.password_val = 'error';
            this.password_two_val = 'error';
            this.messages.push("Введите пароль");
            this.password_two = "";
          }
          else
          {
            if(this.password.length < 9)
            {
              this.password_val = 'error';
              this.password_two_val = 'error';
              this.password = "";
              this.password_two = "";
              this.messages.push("Пароль должен быть не менее 8 символов");
            }
            else
            {

              if(this.password_two === "")
              {
                this.password_two_val = 'error';
                this.messages.push("Подтвердите пароль");
                this.password = "";
              }
              else
                {
                  this.password_two_val = '';
                  this.password_val = '';
                }
            }
          }
        }
        else{
          if(this.password != this.password_two)
          {
            this.password_two_val = 'error';
            this.password_val = 'error';
            this.messages.push("Не удается подтвердить пароль");
            this.password = "";
            this.password_two = "";
          }
          else
          {
            this.password_two_val = '';
            this.password_val = '';
            axios.post('modules/users.php', {method: 'create_user', login: this.login, email: this.email, password: this.password})
            .then(response => {
              if(response.data instanceof Number || typeof response.data === 'number')
                {
                  this.$emit('confirm_key', response.data);
                  this.login = "";
                  this.email = "";
                  this.password = "";
                  this.password_two = "";
                  this.close();
                }
              else
                this.messages.push(response.data)
              ;})
            .catch(e => {alert("error " + e)})
          }
        }
      },
    },
  }
)

/*Vue.component('confirm',
  {
    template:'
    <div class="modal db-example-modal-sm" id="confirm" tabindex="-1" role="dialog" aria-labelledy="advanced-search" :style="hiddened">\
      <div class="modal-dialog modal-sm" role="document">\
        <div class="modal-content">\
          <div class="modal-header">\
            <h5 class="modal-title" id="confirm-label">Подтверждение почты</h5>\
            <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.stop.prevent="close"><span aria-hidden="true">&times;</span></button>\
          </div>\
          <div class="modal-body">\
            <label > Логин </label>\
            <input class="form-control" :class="login_val" type="text" v-model="login"></input>\
            <label > E-mail </label>\
            <input class="form-control" :class="email_val" type="email" v-model="email"></input>\
            <label > Пароль </label>\
            <input class="form-control" :class="password_val" type="password" v-model="password"></input>\
            <label > Подтверждение пароля </label>\
            <input class="form-control" :class="password_two_val" type="password" v-model="password_two"></input>\
          </div>\
          <div class="modal-footer">\
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="add">Добавить</button>\
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="close">Отмена</button>\
          </div>\
        </div>\
      </div>\
    </div>\
    ',
    props: ['key'],
  }
)*/

// Навигационный бар приложения
Vue.component('navbar',
  {
    template:'\
    <div :class="hiddened">\
      <nav class="navbar navbar-expand-lg" :class="theme">\
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">\
          <span class="navbar-toggler-icon"></span>\
        </button>\
        <div class="collapse navbar-collapse" id="navbarSupportedContent">\
          <ul class="navbar-nav mr-auto">\
            <li class="nav-item active" v-show="show_navigate">\
              <a class="nav-link" href="#" @click.stop.prevent="view_main">Главная<span class="sr-only">(current)</span></a>\
            </li>\
            <li class="nav-item" v-show="show_navigate">\
              <a class="nav-link" href="#" @click.stop.prevent="view_garage">Гараж</a>\
            </li>\
            <li class="nav-item dropdown" v-show="show_navigate">\
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                Расходы\
              </a>\
              <div class="dropdown-menu" :class="theme" aria-labelledby="navbarDropdown">\
                <a class="dropdown-item" href="#" @click.stop.prevent="view_gsm">ГСМ</a>\
                <a class="dropdown-item" href="#">Обслуживание</a>\
                <a class="dropdown-item" href="#">Ремонт</a>\
                <a class="dropdown-item" href="#">Прочие расходы</a>\
                <div class="dropdown-divider"></div>\
                <a class="dropdown-item" href="#">Статистика</a>\
              </div>\
            </li>\
            <li class="nav-item" v-show="show_navigate">\
              <a class="nav-link" href="#" @click.stop.prevent="view_settings">Настройки</a>\
            </li>\
          </ul>\
          <form class="form-inline my-2 my-lg-0">\
            <button class="btn btn-outline-danger my-2 my-sm-0" :class="hidden_exit" type="submit" @click.stop.prevent="exit">Выход</button>\
            <button class="btn btn-outline-success my-2 my-sm-0" :class="hidden_login_controls" type="submit" @click.stop.prevent="view_login">Вход</button>\
            <button class="btn btn-outline-warning my-2 my-sm-0" :class="hidden_login_controls" type="submit" @click.stop.prevent="view_registration">Регистрация</button>\
          </form>\
        </div>\
      </nav>\
    </div>\
    ',
    props:['theme_class', 'hidden_navbar', 'hidden_exit_account', 'show_navigate'],
    methods:{
      view_garage:function(){
        this.$emit('show_garage')
      },

      view_main:function(){
        this.$emit('show_main')
      },

      view_settings:function(){
        this.$emit('show_settings')
      },

      view_gsm:function(){
        this.$emit('show_gsm')
      },

      view_registration: function(){
        this.$emit('show_registration')
      },

      view_login: function(){
        this.$emit('show_login');
      },

      exit: function(){
        this.$emit('exit_account');
      },
    },
    computed:{
      hiddened: function(){
        if(this.hidden_navbar === true)
          return 'hidden'
        else
          return ''
      },
      theme: function(){
        if(this.theme_class == "light")
        {
          return 'navbar-light bg-light'
        }
        if(this.theme_class == "dark")
        {
          return 'navbar-light bg-dark'
        }
      },
      hidden_exit: function(){
        if(this.hidden_exit_account === true)
          return 'hidden'
        else
          return ''
      },
      hidden_login_controls: function(){
        if(this.hidden_exit_account === true)
          return ''
        else
          return 'hidden'
      },
    }
  }
)

// Запись таблицы гараж, отображающая один автомобиль
Vue.component('car',
  {
    template:'\
    <tr @click.right.stop.prevent="click_element">\
      <td>{{brand}}</td>\
      <td>{{model}}</td>\
      <td>{{year}}</td>\
      <td>{{mileage}}\
        <button class="btn-outline-warning" :class="hiddened" @click.stop.prevent="edit_car">\
          <img src="../images/edit.ico">\
        </button>\
        <button class="btn-outline-danger" :class="hiddened" @click.stop.prevent="delete_car">\
          <img src="../images/delete.ico">\
        </button>\
      </td>\
    </tr>\
    ',
    props:['id', 'brand', 'model', 'year', 'mileage'],
    data: function(){
      return{
        control_menu: false
      }
    },
    methods:{
      click_element: function(){
        this.control_menu = !this.control_menu;
      },
      edit_car: function(){
        alert(this.id)
      },
      delete_car: function(){
        axios.post('modules/garage.php', {method: 'delete_car', id_user:app.user_id, id_car: this.id})
        .then(response => {alert(response.data);
        this.$emit('refresh_garage')
        })
        .catch(e => {alert("error " + e)})
      }
    },
    computed:{
      hiddened: function(){
        if(this.control_menu === false)
        {
          return 'hidden'
        }
        else {
          return 'btn-my-control btn btn-sm'
        }
      }
    }
  }
)

// Вкладка "Гараж"
Vue.component('garage',
  {
    template:'\
    <div :class="hiddened">\
      <table :class="theme">\
        <thead>\
          <tr>\
            <th scope="col">Марка</th>\
            <th scope="col">Модель</th>\
            <th scope="col">Год выпуска</th>\
            <th scope="col">Пробег</th>\
          </tr>\
        </thead>\
        <tbody>\
          <car v-for="auto, index in garage" :key="index" :id=auto.id :brand=auto.brand :model="auto.model" :year="auto.year" :mileage="auto.mileage" @refresh_garage="refresh_garage"></car>\
        </tbody>\
      </table>\
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" @click.stop.prevent="add_car">Добавить автомобиль</button>\
    </div>\
    ',
    props:['garage', 'hidden_garage', 'theme_class'],
    computed:{
      hiddened: function(){
        if(this.hidden_garage === true)
        {
          return 'hidden'
        }
        else {
          return ''
        }
      },
      theme(){
        if(this.theme_class == 'light')
        {
          return 'table table-hover table-light'
        }
        if(this.theme_class == 'dark')
        {
          return 'table table-hover table-dark'
        }
      }
    },
    methods:{
      add_car: function(){
        this.$emit('add_car')
      },
      refresh_garage: function(){
        this.$emit('refresh_garage')
      }
    }
  }
)

// Вкладка "Настройки"
Vue.component('settings',
  {
    template:'\
    <div :class="hiddened">\
      <button class="btn btn-outline-secondary" @click.stop.prevent="light">Светлая</button>\
      <button class="btn btn-outline-dark" @click.stop.prevent="dark">Темная</button>\
    </div>\
    ',
    props:['hidden_settings'],
    methods:{
      light: function(){
        this.$emit('theme_light')
      },
      dark: function(){
        this.$emit('theme_dark')
      }
    },
    computed:{
      hiddened: function(){
        if(this.hidden_settings === true)
        {
          return 'hidden'
        }
        else {
          return ''
        }
      }
    }
  }
)

// Элемент списка марок автомобилей
Vue.component('car-brand',
  {
    template:'\
    <option>{{brand}}</option>\
    ',
    props: ['brand']
  }
)

// Элемент списка моделей автомобилей
Vue.component('car-model',
  {
    template:'\
    <option>{{model}}</option>\
    ',
    props:['model']
  }
)

// Модальное окно добавления автомобиля
Vue.component('add-car',
  {
    template:'\
    <div class="modal db-example-modal-lg" id="add-car" tabindex="-1" role="dialog" aria-labelledy="advanced-search" :style="hiddened">\
      <div class="modal-dialog modal-lg" role="document">\
        <div class="modal-content">\
          <div class="modal-header">\
            <h5 class="modal-title" id="add-car-label">Добавление автомобиля</h5>\
            <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.stop.prevent="close"><span aria-hidden="true">&times;</span></button>\
          </div>\
          <div class="modal-body">\
            <table class="table">\
              <thead>\
                <tr>\
                  <th scope="col">Марка</th>\
                  <th scope="col">Модель</th>\
                  <th scope="col">Год выпуска</th>\
                  <th scope="col">Пробег</th>\
                </tr>\
              </thead>\
              <tbody>\
                <tr>\
                  <td>\
                    <select class="form-control" :class="brand_val" v-model="brand">\
                      <car-brand v-for="list, index in cars" :key="index" :brand="index"></car-brand>\
                    </select>\
                  </td>\
                  <td>\
                    <select class="form-control" :class="model_val" v-model="model">\
                      <car-model v-for="car, index in cars[brand]" :key="index" :model="car"></car-model>\
                    </select>\
                  </td>\
                  <td><input class="form-control" :class="year_val" type="date" v-model="year"></input></td>\
                  <td><input class="form-control" :class="mileage_val" type="number" placeholder="км" v-model="mileage"></input></td>\
                </tr>\
              </tbody>\
            </table>\
          </div>\
          <div class="modal-footer">\
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="add">Добавить</button>\
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="close">Отмена</button>\
          </div>\
        </div>\
      </div>\
    </div>\
    ',
    props:['hidden_modal_add_car', 'cars', 'user_id'],
    data: function(){
      return {
        brand: "",
        model: "",
        year: "",
        mileage: "",
        brand_val: "",
        model_val: "",
        year_val: "",
        mileage_val: ""
      }
    },

    computed:{
      hiddened: function(){
        if(this.hidden_modal_add_car === true)
        {
          return 'display: none'
        }
        else{
          return 'display: block'
        }
      },

      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      validate: function(){
        if(this.brand_val === 'error' && this.brand != "")
          this.brand_val = ''
        if(this.model_val === 'error' && this.model != "")
          this.model_val = ''
        if(this.year_val === 'error' && this.year != "")
          this.year_val = ''
        if(this.mileage_val === 'error' && this.mileage != "")
          this.mileage_val = ''
      }
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    },
    methods:{
      close: function(){
        this.$emit('close_add_car')
      },
      add: function(){
        if(this.brand === "" || this.model === "" || this.year === "" || this.mileage === "")
        {
          if(this.brand === "")
            this.brand_val = 'error'
          else
            this.brand_val = ''
          if(this.model === "")
            this.model_val = 'error'
          else
            this.model_val = ''
          if(this.year === "")
            this.year_val = 'error'
          else
            this.year_val = ''
          if(this.mileage === "")
            this.mileage_val = 'error'
          else
            this.mileage_val = ''
          alert("Заполнены не все поля")
        }
        else{
          axios.post('modules/garage.php', {method: 'add_car', brand: this.brand, model: this.model, year: this.year, mileage: this.mileage, user: this.user_id})
          .then(response => {alert(response.data);
            this.$emit('car_add_ok');
            this.close();})
          .catch(e => {alert("error " + e)})
        }
      }
    }
  }
)

// Запись таблицы ГСМ
Vue.component('gsm-item',
  {
    template:'\
    <tr>\
      <td>{{car}}</td>\
      <td>{{azs}}</td>\
      <td>{{fuel}}</td>\
      <td>{{price}}</td>\
      <td>{{amount}}</td>\
      <td>{{mileage}}</td>\
      <td>{{cost}}</td>\
    </tr>\
    ',
    props:['car', 'azs', 'fuel', 'price', 'amount', 'mileage', 'cost']
  }
)

// Элемент списка видов топлива
Vue.component('fuel',
  {
    template:'\
    <option>{{fuel}}</option>\
    ',
    props:['fuel']
  }
)

// Вкладка "ГСМ"
Vue.component('gsm',
  {
    template:'\
    <div :class="hiddened">\
      <table :class="theme">\
        <thead>\
          <tr>\
            <th scope="col">Автомобиль</th>\
            <th scope="col">Название АЗС</th>\
            <th scope="col">Вид топлива</th>\
            <th scope="col">Цена (руб.)</th>\
            <th scope="col">Количество (л)</th>\
            <th scope="col">Текущий киллометраж (км)</th>\
            <th scope="col">Стоимость (руб.)</th>\
          </tr>\
        </thead>\
        <tbody>\
          <gsm-item v-for="item, index in gsm" :key="index" :azs="item.azs" :fuel="item.fuel" :price="item.price" :amount="item.amount" :mileage="item.mileage" :cost="calc_cost(item.price, item.amount)"></gsm-item>\
        </tbody>\
      </table>\
      <button class="btn btn-outline-success my-2 my-sm-0" @click.stop.prevent="add_gsm">Добавить заправку</button>\
      <p>Всего: {{calc_total}} руб.</p>\
    </div>\
    ',
    props:['gsm', 'hidden_gsm', 'theme_class'],
    computed:{
      hiddened: function(){
        if(this.hidden_gsm === true)
        {
          return 'hidden'
        }
        else {
          return ''
        }
      },
      theme: function(){
        if(this.theme_class == 'light')
        {
          return 'table table-light'
        }
        if(this.theme_class == 'dark')
        {
          return 'table table-dark'
        }
      },
      calc_total: function(){
        var s = 0
        for(i = 0; i < this.gsm.length; i++)
        {
          s += this.gsm[i].price*this.gsm[i].amount
        }
        return s
      }
    },
    methods:{
      calc_cost: function(price, amount){
        return price * amount
      },
      add_gsm: function(){
        this.$emit('add_gsm_show')
      }
    }
  }
)

// Элемент списка автомобилей имеющихся в гараже
Vue.component('gsm-car',
  {
    template:'\
    <option>{{brand}} {{model}}</option>\
    ',
    props:['brand', 'model']
  }
)

// Модальное окно добавления заправки
Vue.component('add-gsm',
  {
    template:'\
    <div class="modal db-example-modal-lg" id="add-gsm" tabindex="-1" role="dialog" aria-labelledy="advanced-search" :style="hiddened">\
      <div class="modal-dialog modal-lg" role="document">\
        <div class="modal-content">\
          <div class="modal-header">\
            <h5 class="modal-title" id="add-gsm-label">Добавление заправку</h5>\
            <button class="close" type="button" data-dismiss="modal" aria-label="Close" @click.stop.prevent="close"><span aria-hidden="true">&times;</span></button>\
          </div>\
          <div class="modal-body">\
            <table class="table">\
              <thead>\
                <tr>\
                  <th scope="col">Автомобиль</th>\
                  <th scope="col">АЗС</th>\
                  <th scope="col">Вид топлива</th>\
                  <th scope="col">Цена (руб.)</th>\
                  <th scope="col">Количество (л)</th>\
                  <th scope="col">Текущий пробег (км)</th>\
                </tr>\
              </thead>\
              <tbody>\
                <tr>\
                  <td>\
                    <select class="form-control" :class="car_val" v-model="car">\
                      <gsm-car v-for="item, index in garage" :key="index" :brand="item.brand" :model="item.model"></gsm-car>\
                    </select>\
                  </td>\
                  <td>\
                    <input class="form-control" :class="azs_val" type="text" v-model="azs"></input>\
                  </td>\
                  <td>\
                  <select class="form-control" :class="fuel_val" v-model="fuel">\
                    <fuel v-for="item, index in fuels" :key="index" :fuel="item"></fuel>\
                  </select>\
                  </td>\
                  <td><input class="form-control" :class="price_val" type="number" v-model="price"></input></td>\
                  <td><input class="form-control" :class="amount_val" type="number" v-model="amount"></input></td>\
                  <td><input class="form-control" :class="mileage_val" type="number" v-model="mileage"></input></td>\
                </tr>\
              </tbody>\
            </table>\
          </div>\
          <div class="modal-footer">\
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="add">Добавить</button>\
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" data-dismiss="modal" @click.stop.prevent="close">Отмена</button>\
          </div>\
        </div>\
      </div>\
    </div>\
    ',
    props:['hidden_add_gsm', 'fuels', 'garage'],
    data: function(){
      return {
        price_val: "",
        price: "",
        amount_val: "",
        amount: "",
        mileage_val: "",
        mileage: "",
        fuel_val: "",
        fuel: "",
        azs_val: "",
        azs:"",
        car_val: "",
        car: ""
      }
    },
    computed:{
      hiddened: function(){
        if(this.hidden_add_gsm === true)
        {
          return 'display: none'
        }
        else{
          return 'display: block'
        }
      }
    },
    methods:{
      close: function(){
        this.$emit('add_gsm_close')
      },
      add: function(){
        if(this.car === "" || this.azs === "" || this.fuel === "" || this.mileage === "" || this.amount === "" || this.price === "")
        {
          if(this.car === "")
            this.car_val = 'error'
          else
            this.car_val = ''; console.log(this.car)

          if(this.azs === "")
            this.azs_val = 'error'
          else
            this.azs_val = ''

          if(this.fuel === "")
            this.fuel_val = 'error'
          else
            this.fuel_val = ''

          if(this.mileage === "")
            this.mileage_val = 'error'
          else
            this.mileage_val = ''

          if(this.amount === "")
            this.amount_val = 'error'
          else
            this.amount_val = ''

          if(this.price === "")
            this.price_val = 'error'
          else
            this.price_val =''
          alert("Заполнены не все поля")
        }
        else
        {
          axios.post('modules/garage.php', {method: 'add_gsm', car: this.car, azs: this.azs, fuel: this.fuel, price: this.price, amount: this.amount, mileage: this.mileage})
          .then(response => {alert(response.data);
            this.$emit('azs_add_ok');
            this.close();})
          .catch(e => {alert("error " + e)})
        }
      }
    }
  }
)

new Vue({
  el:'#app',
  data:{
    cars:[], // Список марок и моделей автомобилей полученных с сервера
    garage:[], // Список автомобилей в гараже данного пользователя
    hidden_main:false, // Видимость главной страницы
    hidden_garage:true, // Видимость вкладки "Гараж"
    hidden_settings:true, // Видимость вкладки "Настройки"
    theme_class:'light', // Класс цветовой схемы
    hidden_modal_add_car:true, // Видимость модального окна "Добавить автомобиль"
    gsm:[{azs: 'Газпром', fuel: 'АИ-92', price: 40.1, amount: 30, mileage:301237},
    {azs: 'Lukoil', fuel: 'АИ-92', price: 40.5, amount: 27.3, mileage:301237}], // Список заправок данного пользователя
    hidden_gsm:true, // Видимость вкладки "ГСМ"
    hidden_add_gsm:true, // Видимость модального окна "Добавить заправку"
    fuels:[], // Список видов топлива
    hidden_registration:true, // Видимость модальногоокна регистрации
    user_rule: "NOT_AUTHORIZED_USER", // Права пользователя
    user_id: '', // id пользователя
    hidden_login: true, // Видимость модальногоокна входа
    hidden_navbar: false, // Видимость навбара
    hidden_exit_account: true, // Видимость элементов управления аккаунтом
    show_navigate: false, // Видимость элементов навбара
    key_confirm: '',
  },
  methods:{
    // Функция изменяющая права при авторизации пользователя
    rule_user: function(rule, id){
      this.user_rule = rule;
      this.user_id = id;
      this.on_view_garage();
      this.hidden_exit_account = false;
      this.show_navigate = true;
    },

    // Функция определяющая отображение вкладки "Гараж"
    on_view_garage: function(){
      if(this.user_rule === "AUTHORIZED_USER")
      {
        this.hidden_garage = false
        this.hidden_main = true
        this.hidden_settings = true
        this.hidden_gsm = true

        axios.post('modules/garage.php', {method: 'get_garage', id: this.user_id})
        .then(response => {this.garage = []; console.log(response);
          for (i = 0; i < response.data.length; i++)
          {
            this.garage.push({id: response.data[i]['id'], brand: response.data[i]['brand'], model: response.data[i]['model'], year: response.data[i]['year'], mileage: response.data[i]['mileage']});
          }
        })
        .catch(e => {alert("error " + e)})
      }
      else{
        alert("Для использование данного функционала необходимо авторизоваться или зарегистрироваться")
      }
    },

    // Функция определяющая отображение "Главной страницы"
    on_view_main: function(){
      if(this.user_rule != "NOT_AVAILABLE")
      {
        this.hidden_garage = true;
        this.hidden_main = false;
        this.hidden_settings = true;
        this.hidden_gsm = true;
      }
      else{
        this.hidden_navbar = true;
        this.hidden_main = true;
        alert("Нет прав для использования данного ресурса")
      }
    },

    // Функция определяющая отображение вкладки "Настройки"
    on_view_settings: function(){
      if(this.user_rule === "AUTHORIZED_USER")
      {
        this.hidden_garage = true
        this.hidden_main = true
        this.hidden_settings = false
        this.hidden_gsm = true
      }
      else{
        alert("Для использование данного функционала необходимо авторизоваться или зарегистрироваться")
      }
    },

    // Функция определяющая изменение цветовой схемы на светлую
    on_theme_light: function(){
      this.theme_class = 'light'
    },

    // Функция определяющая изменение цветовой схемы на темную
    on_theme_dark: function(){
      this.theme_class = 'dark'
    },

    // Функция определяющая отображение модального окна "Добавить автомобиль в гараж"
    add_car_show: function(){
      this.hidden_modal_add_car = false;
      axios.get('jsons/models-cars.json')
      .then(response => {this.cars = [];
        this.cars = Object.assign({}, response.data);
      })
      .catch(e => {alert("error " + e)})
    },

    // Функция определяющая закрытие модального окна "Добавить автомобиль в гараж"
    on_close_add_car: function(){
      this.hidden_modal_add_car = true
    },

    // Функция определяющая отображение вкладки "ГСМ"
    on_view_gsm: function(){
      if(this.user_rule === "AUTHORIZED_USER")
      {
        this.hidden_garage = true
        this.hidden_main = true
        this.hidden_settings = true
        this.hidden_gsm = false
      }
      else{
        alert("Для использование данного функционала необходимо авторизоваться или зарегистрироваться")
      }
    },

    // Функция определяющая отображение модального окна "Добавить заправку"
    on_show_add_gsm: function(){
      this.hidden_add_gsm = false;

      axios.post('modules/garage.php', {method: 'get_garage'})
      .then(response => {this.garage = [];
        for (i = 0; i < response.data.length; i++)
        {
          this.garage.push({id: response.data[i]['id'], brand: response.data[i]['brand'], model: response.data[i]['model'], year: response.data[i]['year'], mileage: response.data[i]['mileage']});
        }
      })
      .catch(e => {alert("error " + e)})

      axios.get('jsons/fuels.json')
      .then(response => {this.fuels = [];
        this.fuels = Object.assign({}, response.data.fuels);
      })
      .catch(e => {alert("error " + e)})
    },

    // Функция определяющая закрытие модального окна "Добавить заправку"
    on_close_add_gsm: function(){
      this.hidden_add_gsm = true
    },

    // Функция определяющая открытие модального окна "Регистрация"
    on_view_registration: function(){
      this.hidden_registration = false;
    },

    // Функция определяющая закрытие модального окна "Регистрация"
    on_close_registration: function(){
      this.hidden_registration = true;
    },

    // Функция определяющая открытие модального окна входа
    on_view_login: function(){
      this.hidden_login = false;
    },

    // Функция определяющая закрытие модального окна входа
    on_close_login: function(){
      this.hidden_login = true;
    },

    // Функция выполняющая действия при выходе пользователя из аккаунта
    exit_account: function(){
      this.user_rule = "NOT_AUTHORIZED_USER";
      this.hidden_exit_account = true;
      this.theme_class = 'light';
      this.show_navigate = false;
      this.on_view_main();
    },

    confirm_key: function(key){
      this.key_confirm = key;
      alert(this.key_confirm);
    }
  }
})
