<div class="container-fluid">
  <div id="app">
    <header>
      <navbar :show_navigate="show_navigate" :hidden_navbar="hidden_navbar" :theme_class="theme_class" @show_garage="on_view_garage" @show_main="on_view_main" @show_settings="on_view_settings" @show_gsm="on_view_gsm" @show_registration="on_view_registration" @show_login="on_view_login" :hidden_exit_account="hidden_exit_account" @exit_account="exit_account"></navbar>
    </header>
    <add-car :hidden_modal_add_car="hidden_modal_add_car" @close_add_car="on_close_add_car" @car_add_ok="on_view_garage" :cars="cars" :user_id="user_id"></add-car>
    <main-page :hidden_main="hidden_main" :user_rule="user_rule"></main-page>
    <garage :garage="garage" :hidden_garage="hidden_garage" :theme_class="theme_class" @add_car="add_car_show" @refresh_garage="on_view_garage"></garage>
    <settings :hidden_settings="hidden_settings" @theme_light="on_theme_light" @theme_dark="on_theme_dark"></settings>
    <gsm :gsm="gsm" :hidden_gsm="hidden_gsm" :theme_class="theme_class" @add_gsm_show="on_show_add_gsm"></gsm>
    <add-gsm :hidden_add_gsm="hidden_add_gsm" @add_gsm_close="on_close_add_gsm" :fuels="fuels" :garage="garage"></add-gsm>
    <registration :hidden_registration="hidden_registration" @close_registration="on_close_registration" @confirm_key="confirm_key"></registration>
    <login :hidden_login="hidden_login" @close_login="on_close_login" @user_logined="rule_user"></login>
  </div>
</div>
