<?php
////////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////
//         ПЕРЕНЕСТИ ВСЕ ФУНКЦИИ СВЯЗАННЫЕ СО СБОРКОЙ СТРАНИЦЫ В ДРУГОЙ ФАЙЛ     //
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
  class tpl{
    protected $template;
    protected $page = "<!DOCTYPE html>
    <html>"; // код страницы
    protected $head = "<head>";
    protected $body = "<body>";

    // Функция осуществляющая сборку заголовка страницы
    // $title - Заголовок
    // $links - ссылки которые необходимо подключить в виде: array(["rel"=>rel, "href"=>href],[... , ...],...)
    function make_head($title, $links){
      $head_tpl = self::read_tpl('head');
      $head = self::replace_markers($head_tpl, array('{{TITLE}}' => $title));

      while($link = current($links))
      {
        $head .= "<link rel = '" . $link['rel'] . "' href = '" . $link['href'] . "'>";
        next($links);
      }
      $head .= "</head>";
      echo $head;
    }

    // Функция осуществляющая чтение шаблона
    function read_tpl($file_name){
      return file_get_contents("templates/" . $file_name . ".tpl");
    }

    // Замена маркеров в шаблоне
    // $markers = array(marker=>value)
    function replace_markers($tpl, $markers){
      $template = $tpl;
      reset($markers);
      while($marker = current($markers)) {
        $template = str_replace(key($markers), $marker, $template);
        next($markers);
      }
      return $template;
    }

    // Функция осуществляющая сборку тела страницы
    function make_body($markers){
      $body .= self::read_tpl('body');
      if(!empty($markers))
        $body = self::replace_markers($body, $markers);
      $body .= "</body>";
      echo $body;
    }

    // Функция осуществляющая подключение необходимых скриптов
    // $scripts - скрипты которые необходимо подключить в виде: array(["type"=>type, "src"=>src],[... , ...],...)
    function link_scripts($scripts)
    {
      $scripts_code = "";
      while($script = current($scripts))
      {
        $scripts_code .= "<script type = '" . $script['type'] . "' src = '" . $script['src'] . "'></script>";
        next($scripts);
      }
      echo $scripts_code;
    }
  }
 ?>
