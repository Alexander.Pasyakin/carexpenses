<?php
require_once '../config/config.php';
require_once 'db.php';
db::connect();
if(isset($_POST["method"]) && !empty($_POST["method"]))
{
  switch ($_POST["method"]) {
    case 'add_car':
      echo garage::add_car($_POST['user'], $_POST['brand'], $_POST['model'], $_POST['year'], $_POST['mileage']);
      break;
    case 'get_garage':
      garage::get_cars($_POST['id']);
      break;
    case 'delete_car':
      db::del("garage", $_POST["id"]);
      echo "OK";
      break;
    case 'add_gsm':
      db::add_gsm("gsm", $_POST["car"], $_POST["azs"], $_POST["fuel"], $_POST["price"], $_POST["amount"], $_POST["mileage"]);
      echo "Заправка добавлена";
      break;
    default:
      echo "Метод не определен";
      break;
  }
}
else{
  echo "NO";
}

class garage{
  // Функция выполняющая добавление автомобиля в гараж
  //На вход принимает id пользователя, марку, модель, год выпуска и пробег автомобиля
  function add_car($id, $brand, $model, $year, $mileage){
    $query = "SELECT * FROM `" . DB_PREF . "garage` WHERE `id` = '" . $id . "' AND `property` = 'brand'";
    $result = db::query($query);
    $count = $result->num_rows;

    $query = "INSERT INTO `" . DB_PREF . "garage` (`id`, `property`, `index`, `value`)
    VALUES('" . $id . "', 'brand', '" . $count . "', '" . $brand . "'),
    ('" . $id . "', 'model', '" . $count . "', '" . $model . "'),
    ('" . $id . "', 'year', '" . $count . "', '" . $year . "'),
    ('" . $id . "', 'mileage', '" . $count . "', '" . $mileage . "')";

    db::query($query);
      echo "Автомобиль добавлен в гараж";
  }

  // Функция возвращает все автомобили находящиеся у данного пользователя в гараже
  // На вход принимает id пользователя
  function get_cars($id){
    $query = "SELECT * FROM (SELECT * FROM `" . DB_PREF . "garage` WHERE `id` = '" . $id . "') as s";

    $result = db::query($query);

    $count_cars = $result->num_rows / 4;

    $data = array();

    while($row = $result->fetch_assoc())
    {
      $index = $row['index'];
      $brand = $row['value'];
      $row = $result->fetch_assoc();
      $model = $row['value'];
      $row = $result->fetch_assoc();
      $year = $row['value'];
      $row = $result->fetch_assoc();
      $mileage = $row['value'];

      array_push($data, ['id' => $index, 'brand' => $brand, 'model' => $model, 'year' => $year, 'mileage' => $mileage]);
    }

    echo json_encode($data);
  }
}
 ?>
