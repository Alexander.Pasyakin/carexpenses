<?php
  require_once '../config/config.php';
  require_once 'db.php';

  db::connect();
  if(isset($_POST["method"]) && !empty($_POST["method"]))
  {
    switch ($_POST["method"])
    {
      case 'create_user':
        users::create_user($_POST["login"], $_POST["email"], $_POST["password"]);
        break;

      case 'login_user':
        users::login_user($_POST["login"], $_POST["password"]);
        break;
    }
  }

  class users{
    //
    function confirm_key(){
      $key = rand(100000, 999999);

      return $key;
    }

    // Функция регистрации пользователя
    function create_user($login, $email, $password){
      $query = "SELECT * FROM `" . DB_PREF . "users` WHERE `login` = '" . $login . "' OR `e-mail` = '" . $email . "'";

      $result = db::query($query);

      $key = self::confirm_key();

      if($result->num_rows == 0)
      {
        mail($email, 'Регистрация аккаунта в CarExpenses', $key);
        $query = "INSERT INTO `" . DB_PREF . "users` (`login`, `e-mail`, `password`, `rule`, `confirmed`)
        VALUES ('" . $login . "', '" . $email . "', '" . $password . "', '" . AUTHORIZED_USER . "', 'false')";

        db::query($query);
          echo $key;
      }
      else
      {
        $query = "SELECT * FROM `" . DB_PREF . "users` WHERE `login` = '" . $login. "'";
        $result = db::query($query);
        if($result->num_rows > 0)
        {
          echo "Данный логин уже используется другим пользователем";
        }

        $query = "SELECT * FROM `" . DB_PREF . "users` WHERE `e-mail` = '" . $email . "'";
        $result = db::query($query);
        if($result->num_rows > 0)
        {
          echo "Данный e-mail уже используется другим пользователем";
        }
      }
    }

    // Функция авторизации пользователя
    function login_user($login, $password){
      $query = "SELECT `rule`, `id` FROM `" . DB_PREF . "users` WHERE `login` = '" . $login . "' AND `password` = '" . $password . "';";

      $result = db::query($query);

      $row = $result->fetch_assoc();

      $rule = "";

      if($row['rule'] == NOT_AVAILABLE)
        $rule = "NOT_AVAILABLE";
      if($row['rule'] == AUTHORIZED_USER)
        $rule = "AUTHORIZED_USER";
      if($row['rule'] == ADMIN_RULE)
        $rule = "ADMIN_RULE";
      if($row['rule'] == ROOT_RULE)
        $rule = "ROOT_RULE";

      echo json_encode(['id' => $row['id'], 'rule' => $rule]);
    }
  }
 ?>
