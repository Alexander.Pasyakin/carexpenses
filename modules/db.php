<?php
/*****************************************************/
/*Класс описывающий взаимодействие с БД*/
/*****************************************************/
class db{
  static private $db; // объект класса mysqli

  //Подключение к БД
  function connect(){
    self::$db = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
  }

  // Выполнение SQL запрос
  function query($query){
    return self::$db->query($query);
  }

  // Проверка на существование необходимых таблиц в БД
  // и если они не существуют то создать их
  function check_tables(){
    $query = "CREATE TABLE IF NOT EXISTS `" . DB_PREF . "users` (
      `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `login` varchar(50) NOT NULL,
      `password` varchar(50) NOT NULL,
      `e-mail` varchar(50) NOT NULL,
      `rule` varchar(5) Not NULL,
      `confirmed` bool Not NULL
      ) ENGINE=" . DB_ENGINE;

    self::$db->query($query);

    $query = "CREATE TABLE IF NOT EXISTS `" . DB_PREF . "garage` (
      `id` bigint(20)NOT NULL,
      `property` varchar(100) NOT NULL,
      `index` int(10) NOT NULL,
      `value` text ) ENGINE=" . DB_ENGINE;

    self::$db->query($query);

    $query = "CREATE TABLE IF NOT EXISTS `" . DB_PREF . "gsm` (
      `id` bigint(20)NOT NULL,
      `property` varchar(100) NOT NULL,
      `index` int(10) NOT NULL,
      `value` text ) ENGINE=" . DB_ENGINE;

    self::$db->query($query);
  }

  //Добавить запись в БД
  function add_car($tablename, $brand, $model, $year, $mileage){
    $query = "INSERT INTO `" . DB_PREF . $tablename . "` (`brand`, `model`, `year`, `mileage` )
     VALUES ('" . $brand . "', '" . $model . "', '" . $year . "', '" . $mileage . "' )";
    self::$db->query($query);
  }

  function add_gsm($tablename, $car, $azs, $fuel, $price, $amount, $mileage){
    $query = "INSERT INTO `" . DB_PREF . $tablename . "` (`car`, `azs`, `fuel`, `price`, `amount`, `mileage` )
     VALUES ('" . $car . "', '" . $azs . "', '" . $fuel . "', '" . $price . "', '" . $amount . "', '" . $mileage . "' )";
    self::$db->query($query);
  }

  //Получить все записи таблицы
  function get($tablename){
    $result = self::$db->query("SELECT * FROM " . DB_PREF . $tablename);
    return $result;
  }

  function del($table, $id){
    $query = "DELETE FROM `" . DB_PREF . $table . "` WHERE `" . DB_PREF . $table . "`.`id` = " . $id . ";";
    self::$db->query($query);
  }

}
 ?>
